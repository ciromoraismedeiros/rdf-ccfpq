import unittest
from graphs import Graph

dir = 'tests/test_data/weighted_graphs/'


class GraphTest(unittest.TestCase):
    def test_graph_weight(self):
        graph = Graph.load(dir+'graph1.txt', [('weight', float)])
        self.assertAlmostEqual(graph.total_weight(), 12.2)

    def test_read_rdf(self):
        graph = Graph.load('tests/test_data/rdf/skos.rdf')
        # According to RDFLib there are only 144 nodes, but we treat all edge
        # labels as nodes too, so our count is greater.
        self.assertEqual(len(set(graph.all_nodes())), 161)
        # The edge count is the same as in RDFLib.
        self.assertEqual(len(graph), 252)

# TODO: Write more tests
